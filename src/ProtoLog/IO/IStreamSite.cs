using System.IO;

namespace ProtoLog.IO
{
	public interface IStreamSite
	{
		bool SupportsRead { get; }
		
		bool SupportsWrite { get; }
		
		bool SupportsSeek { get; }
		
		Stream GetStream(StreamSiteAccess access);
	}
}

