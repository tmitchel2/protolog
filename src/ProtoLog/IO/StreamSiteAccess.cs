using System;
using System.Runtime.InteropServices;

namespace ProtoLog.IO
{
    [SerializableAttribute]
    [ComVisible(true)]
    [Flags]
	public enum StreamSiteAccess 
	{
		Read,
		Write,
		ReadWrite
	}
}

