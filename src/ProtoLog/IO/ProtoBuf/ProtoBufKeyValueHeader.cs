﻿using ProtoBuf;

namespace ProtoLog.IO.ProtoBuf
{
    [ProtoContract]
    public class ProtoBufKeyValueHeader
    {
        public const int BytesSize = 18;

        [ProtoMember(1, DataFormat = DataFormat.FixedSize, Options = MemberSerializationOptions.Required)]
        public long KeyValueLength;

        [ProtoMember(2, DataFormat = DataFormat.FixedSize, Options = MemberSerializationOptions.Required)]
        public long KeyLength;
    }
}