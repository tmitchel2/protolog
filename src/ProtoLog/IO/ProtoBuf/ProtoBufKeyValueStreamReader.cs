﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ProtoBuf;

namespace ProtoLog.IO.ProtoBuf
{
    public class ProtoBufKeyValueStreamReader<TKey, TValue> : IEnumerator<KeyValuePair<TKey, TValue>>
    {
        private readonly ConstrainedStream _stream;
        private ProtoBufKeyValueHeader _header;
        private TKey _key;
        private TValue _value;

        public ProtoBufKeyValueStreamReader(Stream stream)
        {
            _stream = new ConstrainedStream(stream);
        }

        public TKey Key
        {
            get
            {
                return _key;
            }
        }

        public TValue Value
        {
            get
            {
                if (_value == null)
                {
                    _stream.SetConstraint(_stream.BaseStream.Position, _header.KeyValueLength - _header.KeyLength);
                    _value = Serializer.Deserialize<TValue>(_stream);
                }
                return _value;
            }
        }

        public bool MoveNext()
        {
            // Push past the value if it was never deserialised
            if (_header != null && _value == null)
                _stream.SetConstraint(_stream.BaseStream.Position + _header.KeyValueLength - _header.KeyLength, 0);

            _key = default(TKey);
            _value = default(TValue);

            if (_stream.BaseStream.Position >= _stream.BaseStream.Length)
                return false;

            _stream.SetConstraint(_stream.BaseStream.Position, ProtoBufKeyValueHeader.BytesSize);
            _header = Serializer.Deserialize<ProtoBufKeyValueHeader>(_stream);

            _stream.SetConstraint(_stream.BaseStream.Position, _header.KeyLength);
            _key = Serializer.Deserialize<TKey>(_stream);

            return true;
        }

        public void Reset()
        {
            throw new NotSupportedException();
        }

        public KeyValuePair<TKey, TValue> Current
        {
            get { return new KeyValuePair<TKey, TValue>(Key, Value); }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        public void Dispose()
        {
            if (_stream != null)
                _stream.Dispose();
        }
    }
}