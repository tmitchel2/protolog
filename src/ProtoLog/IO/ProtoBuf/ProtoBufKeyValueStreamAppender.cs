﻿/*
using System;
using System.Collections.Generic;
using System.IO;
using ProtoBuf;
using ProtoLog.Collections;

namespace ProtoLog.IO.ProtoBuf
{
    public class ProtoBufKeyValueStreamAppender<TKey, TValue> : IAppender<KeyValuePair<TKey, TValue>>
    {
        private readonly Stream _stream;

        public ProtoBufKeyValueStreamAppender(Stream stream)
        {
            _stream = stream;
        }

        public long Append(KeyValuePair<TKey, TValue> item)
        {
			var pos = _stream.Position;
            using (var keyStream = new MemoryStream())
            using (var valueStream = new MemoryStream())
            {
                Serializer.Serialize(keyStream, item.Key);
                Serializer.Serialize(valueStream, item.Value);
                Serializer.Serialize(
                    _stream,
                    new ProtoBufKeyValueHeader
                    {
                        KeyLength = keyStream.Length,
                        KeyValueLength = keyStream.Length + valueStream.Length
                    });
                keyStream.Position = 0;
                valueStream.Position = 0;
                keyStream.CopyTo(_stream);
                valueStream.CopyTo(_stream);
            }
			return pos;
        }

        public void Dispose()
        {

        }
    }
}
*/