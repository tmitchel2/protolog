﻿/*
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ProtoLog.Collections;

namespace ProtoLog.IO.ProtoBuf
{
    public class ProtoBufKeyValueAppendableEnumerable<TKey, TValue> 
        : IAppendableEnumerable<KeyValuePair<TKey, TValue>>
    {
		private readonly Stream _stream;
		
		public ProtoBufKeyValueAppendableEnumerable(Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException("stream");
			
			_stream = stream;
		}
		
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return new ProtoBufKeyValueStreamReader<TKey, TValue>(_stream);
        }

        public IAppender<KeyValuePair<TKey, TValue>> GetAppender()
        {
            return new ProtoBufKeyValueStreamAppender<TKey, TValue>(_stream);
        }

		IEnumerator IEnumerable.GetEnumerator ()
		{
			throw new System.NotImplementedException ();
		}

		public void Dispose ()
		{
			_stream.Dispose();
		}
    }
}
*/