﻿using System;
using System.IO;

namespace ProtoLog.IO
{
    public class ConstrainedStream : Stream
    {
        private readonly Stream _baseStream;
        private long _start;
        private long _length;

        public ConstrainedStream(Stream baseStream)
        {
            _baseStream = baseStream;
        }

        public Stream BaseStream
        {
            get { return _baseStream; }
        }

        public void SetConstraint(long start)
        {
            _start = start;
            _length = -1;
            _baseStream.Position = _start;
        }

        public void SetConstraint(long start, long length)
        {
            if (length < 0)
                throw new ArgumentOutOfRangeException("length", "Must be greater or equal to zero.");

            _start = start;
            _length = length;
            _baseStream.Position = _start;
        }

        public override void Flush()
        {
            throw new NotSupportedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _baseStream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (count > (Length - Position))
                count = (int)(Length - Position);

            var outCount = _baseStream.Read(buffer, offset, count);
            return outCount;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        public override bool CanRead
        {
            get { return _baseStream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return _baseStream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override long Length
        {
            get
            {
                if (_length == -1)
                    return _baseStream.Length - _start;

                return _length;
            }
        }

        public override long Position
        {
            get
            {
                return _baseStream.Position - _start;
            }
            set
            {
                _baseStream.Position = value + _start;
            }
        }
    }
}