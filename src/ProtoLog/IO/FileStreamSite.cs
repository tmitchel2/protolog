using System;
using System.IO;

namespace ProtoLog.IO
{

	public class FileStreamSite : IStreamSite
	{
		private string _path;
		
		public FileStreamSite(string path)
		{
			if (string.IsNullOrWhiteSpace(path))
				throw new ArgumentException("path");
			
			_path = path;
		}

		public bool SupportsRead 
		{
			get 
			{
				return true;
			}
		}

		public bool SupportsWrite 
		{
			get 
			{
				return true;
			}
		}

		public bool SupportsSeek 
		{
			get 
			{
				return true;
			}
		}
		
		public Stream GetStream (StreamSiteAccess access)
		{
			return new FileStream(_path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
		}
	}
}

