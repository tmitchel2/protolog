using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using ProtoLog.Collections;
using ProtoLog.IO;

namespace ProtoLog.Tests.Collections
{
	[TestFixture]
	public abstract class JournalTests
	{
		[Test]
		public void AppendWorks()
		{
			var fileSite = new FileStreamSite(Path.Combine(Path.GetTempPath(), Guid.NewGuid() + "_Journal.bin"));
            using (var journal = CreateJournal<TestKey, TestValue>(fileSite))
            {
                const int itemCount = 10;
				for (int i = 0; i < itemCount; i++)
				{
					journal.Append(new TestKey { MyKey = i }, new TestValue { MyValue = "This is item " + i });
				}

			    Assert.AreEqual(itemCount, journal.Count());
			}
		}

        [Test]
        public void GetEnumeratorWorks()
        {
            var fileSite = new FileStreamSite(Path.Combine(Path.GetTempPath(), Guid.NewGuid() + "_Journal.bin"));
            using (var journal = CreateJournal<TestKey, TestValue>(fileSite))
            {
                const int itemCount = 10;
                for (int i = 0; i < itemCount; i++)
                {
                    journal.Append(new TestKey { MyKey = i }, new TestValue { MyValue = "This is item " + i });
                }

                var items = journal.ToList();
                for (int i = 0; i < itemCount; i++)
                {
                    Assert.AreEqual(i, items[i].Key.MyKey);
                    Assert.AreEqual("This is item " + i, items[i].Value.MyValue);
                }
            }
        }

        [Test]
        public void ContainsKeyWorks()
        {
            var fileSite = new FileStreamSite(Path.Combine(Path.GetTempPath(), Guid.NewGuid() + "_Journal.bin"));
            using (var journal = CreateJournal<TestKey, TestValue>(fileSite))
            {
                const int itemCount = 10;
                for (int i = 0; i < itemCount; i++)
                {
                    journal.Append(new TestKey { MyKey = i }, new TestValue { MyValue = "This is item " + i });
                }

                for (int i = 0; i < itemCount; i++)
                {
                    Assert.IsTrue(journal.ContainsKey(new TestKey {MyKey = i}));
                }
                Assert.IsFalse(journal.ContainsKey(new TestKey { MyKey = itemCount + 1 }));
            }
        }

        protected abstract IJournal<TKey, TValue> CreateJournal<TKey, TValue>(IStreamSite streamSite);
	}
}

