﻿using System;
using ProtoBuf;

namespace ProtoLog.Tests.IO
{
    [ProtoContract]
    public class SequenceItem : IComparable<SequenceItem>, IEquatable<SequenceItem>
    {
        [ProtoMember(1)]
        public string Data;

        public int CompareTo(SequenceItem other)
        {
            if (other == null)
                return 1;

            if (Data == null && other.Data == null)
                return 0;

            if (Data == null)
                return -1;

            return Data.CompareTo(other.Data);
        }

        public bool Equals(SequenceItem other)
        {
            if (other == null)
                return false;

            if (Data == null && other.Data == null)
                return true;

            if (Data == null || other.Data == null)
                return false;

            return Data.Equals(other.Data);
        }

        public override string ToString()
        {
            return Data;
        }
    }
}