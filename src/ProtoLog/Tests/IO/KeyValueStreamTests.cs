﻿/*
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NUnit.Framework;
using ProtoLog.Collections;
using ProtoLog.IO.ProtoBuf;

namespace ProtoLog.Tests.IO
{
    [TestFixture]
    public class KeyValueStreamTests
    {
        [Test]
        public void DetermineSequenceItemReadWriteSeekTimes()
        {
            WriteSerialisationPerformanceToConsole(
                i => new KeyValuePair<int, SequenceItem>(i, new SequenceItem {Data = "This is record " + i}));
        }

        private static IAppender<KeyValuePair<TKey, TValue>> CreateWriter<TKey, TValue>(Stream stream)
        {
            return new ProtoBufKeyValueStreamAppender<TKey, TValue>(stream);
        }

        private static IEnumerator<KeyValuePair<TKey, TValue>> CreateReader<TKey, TValue>(Stream stream)
        {
            return new ProtoBufKeyValueStreamReader<TKey, TValue>(stream);
        }

        private static void WriteSerialisationPerformanceToConsole<TKey, TValue>(
            Func<int, KeyValuePair<TKey, TValue>> createItemDelegate)
        {
            // Create items
            var items = Enumerable
                .Range(1, 10000)
                .Select(createItemDelegate.Invoke)
                .ToArray();

            using (var ms = new MemoryStream())
            {
                // Write items
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                using (var writer = CreateWriter<TKey, TValue>(ms))
                {
                    foreach (var item in items)
                        writer.Append(new KeyValuePair<TKey, TValue>(item.Key, item.Value));
                }
                stopwatch.Stop();
                ms.Position = 0;
                Console.WriteLine("{0} ms to write {1} data items (WRITE - {2} items/s)", stopwatch.ElapsedMilliseconds, items.Length, (items.Length / stopwatch.ElapsedMilliseconds) * 1000);

                // Read items
                stopwatch.Restart();
                var newItems = new List<TValue>();
                using (var reader = CreateReader<TKey, TValue>(ms))
                {
                    while (reader.MoveNext())
                        newItems.Add(reader.Current.Value);
                }
                stopwatch.Stop();
                ms.Position = 0;
                Console.WriteLine("{0} ms to read {1} data items  (READ - {2} items/s)", stopwatch.ElapsedMilliseconds, items.Length, (items.Length / stopwatch.ElapsedMilliseconds) * 1000);

                // Seek to item
                stopwatch.Restart();
				TValue newItem;
                using (var reader = CreateReader<TKey, TValue>(ms))
                {
                    for (int i = 0; i < items.Length / 2; i++ )
                        reader.MoveNext();

                    newItem = reader.Current.Value;
                }
                stopwatch.Stop();
                Console.WriteLine("{0} ms to seek {1} data items    (SEEK - {2} items/s)", stopwatch.ElapsedMilliseconds, items.Length / 2, ((items.Length / 2) / stopwatch.ElapsedMilliseconds) * 1000);
                Assert.IsNotNull(newItem);
            }
        }
    }
}
*/