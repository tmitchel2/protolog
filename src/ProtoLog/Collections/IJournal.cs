using System;
using System.Collections.Generic;

namespace ProtoLog.Collections
{
	/// <summary>
	/// A Journal is an append only collection of items which are indexed by a specified type.
	/// </summary>
	public interface IJournal<TKey, TValue>
		: IEnumerable<KeyValuePair<TKey, TValue>>
		, IDisposable
	{
		/// <summary>
		/// Gets a value indicating whether this instance is read only.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is read only; otherwise, <c>false</c>.
		/// </value>
		bool IsReadOnly { get; }
		
		TValue this[TKey key]
		{ 
			get;
		}
		
		/// <summary>
		/// Gets the keys.
		/// </summary>
		/// <value>
		/// The keys.
		/// </value>
		IEnumerable<TKey> Keys { get; }
		
		/// <summary>
		/// Gets the values.
		/// </summary>
		/// <value>
		/// The values.
		/// </value>
		IEnumerable<TValue> Values { get; }
		
		/// <summary>
		/// Append the specified key and value.
		/// </summary>
		/// <param name='key'>
		/// Key.
		/// </param>
		/// <param name='value'>
		/// Value.
		/// </param>
		long Append(TKey key, TValue value);
		
		/// <summary>
		/// Containses the key.
		/// </summary>
		/// <returns>
		/// The key.
		/// </returns>
		/// <param name='key'>
		/// If set to <c>true</c> key.
		/// </param>
		bool ContainsKey(TKey key);
        
		/// <summary>
		/// Tries the get value.
		/// </summary>
		/// <returns>
		/// The get value.
		/// </returns>
		/// <param name='key'>
		/// If set to <c>true</c> key.
		/// </param>
		/// <param name='value'>
		/// If set to <c>true</c> value.
		/// </param>
		bool TryGetValue(TKey key, out TValue value);
	}
}

