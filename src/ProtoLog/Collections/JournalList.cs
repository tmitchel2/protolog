namespace ProtoLog.Collections
{
	/// <summary>
	/// Journal list.
	/// </summary>
	public class JournalList<T> : JournalDictionary<long, T>
	{
		public JournalList(
			IJournal<long, long> indexJournal,
            IJournal<long, T> dataJournal)
			: base(indexJournal, dataJournal)
		{
		}
	}
}

