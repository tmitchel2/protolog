namespace ProtoLog.Collections
{
	/// <summary>
	/// Journal set.
	/// </summary>
	public class JournalSet<T> : JournalDictionary<T, NullObject>
	{
		public JournalSet(
			IJournal<T, long> indexJournal,
            IJournal<T, NullObject> dataJournal)
			: base(indexJournal, dataJournal)
		{
		}
	}
}

