using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ProtoLog.IO;
using ProtoLog.IO.ProtoBuf;
using ProtoBuf;

namespace ProtoLog.Collections.Protobuf
{
	/// <summary>
	/// Protobuf journal enumerator.
	/// </summary>
	public class ProtobufJournalEnumerator<TKey, TValue> : IEnumerator<KeyValuePair<TKey,TValue>>
	{
		private readonly ConstrainedStream _stream;
		private ProtoBufKeyValueHeader _header;
		private TKey _key;
		private TValue _value;
	    private bool _valueDeserialized;

	    public ProtobufJournalEnumerator(Stream stream)
		{
			_stream = new ConstrainedStream(stream);
		}
	
		/// <summary>
		/// Gets the key.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		public TKey Key
		{
			get
			{
				return _key;
			}
		}
	
		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public TValue Value
		{
			get
			{
				if (!_valueDeserialized)
				{
					_stream.SetConstraint(_stream.BaseStream.Position, _header.KeyValueLength - _header.KeyLength);
					_value = Serializer.Deserialize<TValue>(_stream);
				    _valueDeserialized = true;
				}
				return _value;
			}
		}
	
		public bool MoveNext()
		{
			// Push past the value if it was never deserialised
            if (_header != null && !_valueDeserialized)
			{
				_stream.SetConstraint(_stream.BaseStream.Position + _header.KeyValueLength - _header.KeyLength, 0);
			}
	
			_key = default(TKey);
			_value = default(TValue);
	
			if (_stream.BaseStream.Position >= _stream.BaseStream.Length)
			{
				return false;
			}
	
			_stream.SetConstraint(_stream.BaseStream.Position, ProtoBufKeyValueHeader.BytesSize);
			_header = Serializer.Deserialize<ProtoBufKeyValueHeader>(_stream);
	
			_stream.SetConstraint(_stream.BaseStream.Position, _header.KeyLength);
			_key = Serializer.Deserialize<TKey>(_stream);
            
            _valueDeserialized = false;
			return true;
		}
	
		public void Reset()
		{
			throw new NotSupportedException();
		}
	
		public KeyValuePair<TKey, TValue> Current
		{
			get { return new KeyValuePair<TKey, TValue>(Key, Value); }
		}
	
		public void Dispose()
		{
			if (_stream != null)
			{
				_stream.Dispose();
			}
		}

		object IEnumerator.Current
		{
			get
			{
				throw new System.NotImplementedException();
			}
		}
	}

}

