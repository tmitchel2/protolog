using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ProtoLog.IO;
using ProtoLog.IO.ProtoBuf;
using ProtoBuf;

namespace ProtoLog.Collections.Protobuf
{	
	/// <summary>
	/// Protobuf journal is a protobuf implementation of the IJournal interface.
	/// </summary>
	public class ProtobufJournal<TKey, TValue> : IJournal<TKey, TValue>
	{		
		private readonly IStreamSite _streamSite;
		private readonly Stream _appendStream;

		public ProtobufJournal(IStreamSite streamSite)
		{
			if (streamSite == null)
			{
				throw new ArgumentNullException("streamSite");
			}
			
			_streamSite = streamSite;
			_appendStream = _streamSite.GetStream(StreamSiteAccess.ReadWrite);
		}
		
		/// <summary>
		/// Append the specified key and value.
		/// </summary>
		/// <param name='key'>
		/// Key.
		/// </param>
		/// <param name='value'>
		/// Value.
		/// </param>
		public long Append(TKey key, TValue value)
		{
			var pos = _appendStream.Position;
			using (var keyStream = new MemoryStream())
			{
			using (var valueStream = new MemoryStream())
			{
				Serializer.Serialize(keyStream, key);
				Serializer.Serialize(valueStream, value);
				Serializer.Serialize(
                    _appendStream,
                    new ProtoBufKeyValueHeader
                    {
                        KeyLength = keyStream.Length,
                        KeyValueLength = keyStream.Length + valueStream.Length
                    });
				keyStream.Position = 0;
				valueStream.Position = 0;
				keyStream.CopyTo(_appendStream);
				valueStream.CopyTo(_appendStream);
                _appendStream.Flush();
			}
			}
			return pos;
		}
		
		/// <summary>
		/// Containses the key.
		/// </summary>
		/// <returns>
		/// The key.
		/// </returns>
		/// <param name='key'>
		/// If set to <c>true</c> key.
		/// </param>
		public bool ContainsKey(TKey key)
		{
			return this.Where(f => f.Key.Equals(key)).Any();
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read only.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is read only; otherwise, <c>false</c>.
		/// </value>
		public bool IsReadOnly
		{
			get
			{
				return !_streamSite.SupportsWrite;
			}
		}

		public void Dispose()
		{
			_appendStream.Dispose();
		}
		
		/// <summary>
		/// Gets the enumerator.
		/// </summary>
		/// <returns>
		/// The enumerator.
		/// </returns>
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			return new ProtobufJournalEnumerator<TKey, TValue>(_streamSite.GetStream(StreamSiteAccess.ReadWrite));
		}
		
		/// <summary>
		/// Gets the enumerator.
		/// </summary>
		/// <returns>
		/// The enumerator.
		/// </returns>
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		
		/// <summary>
		/// Tries the get value.
		/// </summary>
		/// <returns>
		/// The get value.
		/// </returns>
		/// <param name='key'>
		/// If set to <c>true</c> key.
		/// </param>
		/// <param name='value'>
		/// If set to <c>true</c> value.
		/// </param>
		public bool TryGetValue(TKey key, out TValue value)
		{
			foreach (var item in this)
			{
				if (item.Key.Equals(key))
				{
					value = item.Value;
					return true;
				}
			}
			value = default(TValue);
			return false;
		}

		public TValue this[TKey key]
		{
			get
			{
				TValue value;
				if (TryGetValue(key, out value))
				{
					return value;
				}
				throw new KeyNotFoundException();
			}
		}
		
		/// <summary>
		/// Gets the keys.
		/// </summary>
		/// <value>
		/// The keys.
		/// </value>
		public IEnumerable<TKey> Keys
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets the values.
		/// </summary>
		/// <value>
		/// The values.
		/// </value>
		public IEnumerable<TValue> Values
		{
			get
			{
				throw new NotImplementedException();
			}
		}
		
		/// <summary>
		/// Gets the enumerator.
		/// </summary>
		/// <returns/>
		/// The enumerator.
		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}

