using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ProtoLog.Collections
{
	/// <summary>
	/// Journal dictionary.
	/// </summary>
	public class JournalDictionary<TKey, TValue> 
        : IJournal<TKey, TValue>
	{
		class JournalDictionaryEnumerator : IEnumerator<KeyValuePair<TKey, TValue>>
		{
			private readonly IEnumerator<KeyValuePair<TKey, long>> _indexReader;
			private readonly IEnumerator<KeyValuePair<TKey, TValue>> _dataReader;

			public JournalDictionaryEnumerator(IEnumerator<KeyValuePair<TKey, long>> indexReader, IEnumerator<KeyValuePair<TKey, TValue>> dataReader)
			{
				_indexReader = indexReader;
				_dataReader = dataReader;
			}

			public void Dispose()
			{
				_indexReader.Dispose();
				_dataReader.Dispose();
			}

			public bool MoveNext()
			{
				throw new NotImplementedException();
			}

			public void Reset()
			{
				throw new NotImplementedException();
			}

			public KeyValuePair<TKey, TValue> Current
			{
				get { throw new NotImplementedException(); }
			}

			object IEnumerator.Current
			{
				get { return Current; }
			}
		}

		private readonly IJournal<TKey, long> _indexJournal;
		private readonly IJournal<TKey, TValue> _dataJournal;
		private readonly List<TKey> _indexKeys;
		private readonly List<long> _indexValues;
       
		public JournalDictionary(
			IJournal<TKey, long> indexJournal,
            IJournal<TKey, TValue> dataJournal)
		{
			if (indexJournal == null)
			{
				throw new ArgumentNullException("indexJournal");
			}
			if (dataJournal == null)
			{
				throw new ArgumentNullException("dataJournal");
			}

			_indexJournal = indexJournal;
			_dataJournal = dataJournal;
			_indexKeys = new List<TKey>();
			_indexValues = new List<long>();
            
			using (var reader = _indexJournal.GetEnumerator())
			{
				TKey currentKey = default(TKey);
				while (reader.MoveNext())
				{
					if (!EqualityComparer<TKey>.Default.Equals(default(TKey), currentKey) && Comparer<TKey>.Default.Compare(reader.Current.Key, currentKey) < 0)
					{
						throw new IOException(string.Format("The key is out of order.  Key {0} is after {1}.", reader.Current.Key, currentKey));
					}

					_indexKeys.Add(reader.Current.Key);
					_indexValues.Add(reader.Current.Value);
				}
			}
		}

		public long Append(TKey key, TValue value)
		{
			var pos = _dataJournal.Append(key, value);
			_indexJournal.Append(key, pos);
			_indexKeys.Add(key);
			_indexValues.Add(pos);
			return pos;
		}

		public bool ContainsKey(TKey key)
		{
			return _indexKeys.BinarySearch(key) >= 0;
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			var keyIndex = _indexKeys.BinarySearch(key);
			if (keyIndex < 0)
			{
				value = default(TValue);
				return false;
			}

			var indexValue = _indexValues[keyIndex];
			//_enumerator.DataStream.SetConstraint(indexValue);
			//value = _enumerator.Current.Value;
			value = default(TValue);
			return true;
		}

		public TValue this[TKey key]
		{
			get
			{
				TValue value;
				if (!TryGetValue(key, out value))
				{
					throw new KeyNotFoundException();
				}

				return value;
			}
		}

		public void Dispose()
		{
			_indexJournal.Dispose();
			_dataJournal.Dispose();
		}
		
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator(KeyValuePair<TKey, TValue> start)
		{
			throw new NotImplementedException();
		}

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator(KeyValuePair<TKey, TValue> start, KeyValuePair<TKey, TValue> finish)
		{
			throw new NotImplementedException();
		}

		public bool IsReadOnly
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public IEnumerable<TKey> Keys
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public IEnumerable<TValue> Values
		{
			get
			{
				throw new NotImplementedException();
			}
		}

	}

}

