﻿using System.Collections.Generic;

namespace ProtoLog.Collections
{
	/// <summary>
	/// I enumerable range.
	/// </summary>
	public interface IEnumerableRange<TIndexer, TValue> : IEnumerable<TValue>
	{
		/// <summary>
		/// Gets the enumerator.
		/// </summary>
		/// <returns>
		/// The enumerator.
		/// </returns>
		/// <param name='start'>
		/// Start.
		/// </param>
		IEnumerator<TValue> GetEnumerator(TIndexer start);
        
		/// <summary>
		/// Gets the enumerator.
		/// </summary>
		/// <returns>
		/// The enumerator.
		/// </returns>
		/// <param name='start'>
		/// Start.
		/// </param>
		/// <param name='finish'>
		/// Finish.
		/// </param>
		IEnumerator<TValue> GetEnumerator(TIndexer start, TIndexer finish);
	}
}